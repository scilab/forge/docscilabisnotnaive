Documentation : Scilab is Not Naive

Abstract

Most of the time, the mathematical formula is directly used in the 
Scilab source code. But, in many algorithms, some additional work is 
performed, which takes into account the fact that the computer does not 
process mathematical real values, but performs computations with their 
floating point representation. The goal of this article is to show that, 
in many situations, Scilab is not naive and use algorithms which have been 
specifically tailored for floating point computers. We analyze in this 
article the particular case of the quadratic equation, the complex 
division and the numerical derivatives. In each example, we show that 
the naive algorithm is not sufficiently accurate, while Scilab 
implementation is much more robust. 

Author

Copyright (C) 2008-2010 - Consortium Scilab - Digiteo - Michael Baudin

Licence

This document is released under the terms of the Creative Commons Attribution-ShareAlike 3.0 Unported License :

http://creativecommons.org/licenses/by-sa/3.0/

