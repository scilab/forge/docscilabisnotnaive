// Copyright (C) 2009 - Michael Baudin
//
// mynorm2.sce --
//   Compare the Euclidian  norma computation 
//   with vector.
//
//
// References :
// http://en.wikipedia.org/wiki/Methods_of_computing_square_roots
// http://mathworld.wolfram.com/SquareRootAlgorithms.html
//
//  Replacing Square Roots by Pythagorean Sums
// Cleve Moler and Donald Morrison
// http://www.research.ibm.com/journal/rd/276/ibmrd2706P.pdf
//

// Straitforward implementation
function mn2 = mynorm2(a,b)
  mn2 = sqrt(a^2+b^2)
endfunction
// With scaling
function mn2 = mypythag1(a,b)
  if (a==0.0) then
    mn2 = abs(b);
  elseif (b==0.0) then
    mn2 = abs(a);
  else
    if (abs(b)>abs(a)) then
      r = a/b;
      t = abs(b);
    else
      r = b/a;
      t = abs(a);
    end
    mn2 = t * sqrt(1 + r^2);
  end
endfunction
// With Moler & Morrison's
// At most 7 iterations are required.
function mn2 = mypythag2(a,b)
  p = max(abs(a),abs(b))
  q = min(abs(a),abs(b))
  //index = 0
  while (q<>0.0)
    //index = index + 1
    //mprintf("index = %d, p = %e, q = %e\n",index,p,q)
    r = (q/p)^2
    s = r/(4+r)
    p = p + 2*s*p
    q = s * q
  end
  mn2 = p
endfunction
function compare(x)
  mprintf("x(1)=%e, x(2)=%e\n",x(1),x(2));
  p = norm(x,2);
  mprintf("%20s : %e\n","Scilab",p);
  p = mynorm2(x(1),x(2));
  mprintf("%20s : %e\n","Naive",p);
  p = mypythag1(x(1),x(2));
  mprintf("%20s : %e\n","Scaling",p);
  p = mypythag2(x(1),x(2));
  mprintf("%20s : %e\n","Moler & Morrison",p);
  p = normpatched(x,2);
  mprintf("%20s : %e\n","Patch",p);
endfunction
function y=normpatched(A,flag)
//compute various matrix norms
if argn(2)==1 then flag=2,end

if type(A)==1 then
  if A==[] then y=0,return,end
  if or(size(A)==1) then // vector norm
    if type(flag)==10 then //'inf' or 'fro'
      select convstr(part(flag,1))
      case 'i' then //'inf'
	y=max(abs(A))
      case 'f' then //'fro'
	A=A(:)
	y=sqrt(A'*A)
      else
	error("invalid value for flag")
      end
    elseif type(flag)==1 then //p_norm
      p=flag;
      if ~isreal(p) then
	error('flag must be real')
      end
      if p==%inf then
	y=max(abs(A))
      elseif p==1 then
	y=sum(abs(A))
      elseif p==-%inf then
	y=min(abs(A))
      elseif isnan(p) then
	y=%nan
      elseif p==0 then
	y=%inf
      else
	//y=sum(abs(A).^p)^(1/p)
        s = max(abs(A));
        y=s * sum(abs(A/s).^p)^(1/p)
      end
    else
      error("invalid value for flag")
    end
  else //matrix norm
    if type(flag)==10 then //'inf' or 'fro'
      select convstr(part(flag,1))
      case 'i' then //'inf'
	y=max(sum(abs(A),2))  
	case 'f' then //'fro'
	if size(A,1)>size(A,2) then
	  y=sqrt(sum(diag(A'*A))) 
	else
	  y=sqrt(sum(diag(A*A'))) 
	end
      else
	error("invalid value for flag")
      end
    elseif type(flag)==1 then //p_norm
      p=flag;
      select p
	case 1 then 
	y=max(sum(abs(A),1))
	case 2 then
	y=max(svd(A))
	case %inf then
	y=max(sum(abs(A),2))  
      else
	error('flag must be 1 2 or inf')
      end
    else
      error("invalid value for flag")
    end    
  end
else
  if type(A)==16|type(A)==17 then
    n=getfield(1,A);n=n(1)
  else
    [t,n]=typename()
    n=stripblanks(n(find(t==type(A))))
  end
  fun='%'+n+'_norm'
  if exists(fun)==1 then
    execstr('y='+fun+'(A,flag)')
  else
    error('norm not defined for type ""'+n+'"" .'+..
	  'Check argument or define function '+fun)
  end
end
endfunction
// Test #1 : all is fine
x = [1 1]
compare(x);
// Test #2 : more difficult when x is large
x = [1.e200 1]
compare(x)
// Test #3 : more difficult when x is small
x = [1.e-200 1.e-200]
compare(x)
// Timings
x= ones(1000000,1);
tic;norm(x,2);toc
tic;normpatched(x,2);toc

