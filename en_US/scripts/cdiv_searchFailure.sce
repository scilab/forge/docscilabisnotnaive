// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

// In this script, we search failure cases for all algorithms.
// I notice that Scilab/x87 produce correct results in the 
// experiments we checked. 
// I think that this is caused by the extended precision of the x87 registers
// which are used by Scilab on this Linux 32 bits system.
// Hence, I put Scilab/x87 (i.e. cdiv_scilab) as the reference.
// I try divisions defined by [a,b,c,d]=[2^na,2^nb,2^nc,2^nd],
// where na,nb,nc,nd are in [-1074 1023].
// I perform kmax such divisions, with kmax a large integer, 
// kmax=100 000 for example.
// I count the number of times k a particular algorithm, say Smith for example,
// produced a different result (and non-nan, non-inf).
// I assume that Scilab/x87 is always correct, so that k 
// is a count of the failures.
// The probability of failure is then p=k/kmax.
// I think that this may be a reasonable measure of failure probability,
// because failures in the division are most of the times caused 
// by an underflow/overflow in a multiplication/division.
// Hence, there should be a very small number of failure caused 
// by overflow in an addition.
//
// The results:
// Naive   vs Scilab/x87 : p=1.220e-1
// Smith   vs Scilab/x87 : p=1.305e-2
// SmithLi vs Scilab/x87 : p=1.312e-2
// Smith4  vs Scilab/x87 : p=2.220e-4
// Smith5  vs Scilab/x87 : p=2.400e-4



// Some attempts to improve Smith's algorithm

haveimproved = %t;

// cdiv_naive.sci : a naive method
exec("cdiv_naive.sci");

// cdiv_smith.sci : original Smith's method
exec("cdiv_smith.sci");

// An improved Smith method, with ideas from Stewart (prodminmax)
exec("cdiv_smith2.sci");

// An improved Smith method, with an exp-log product
exec("cdiv_smith3.sci");

if ( haveimproved ) then
  // An improved Smith method
  exec("cdiv_smith4.sci");
  // An improved Smith, with ideas from Li et al.
  exec("cdiv_smith5.sci");
  // An improved Smith, with ideas from Li et al.
  exec("cdiv_smith6.sci");
end

// A Scilab port of the ANSI/ISO C algorithm
exec("cdiv_ansiisoC.sci");

// A Scilab port of the Li et al. improved Smith
exec("cdiv_smithLi.sci");

exec("assert_computedigits.sci");

ieee(2);

function r = cdiv_scilab ( x , y )
  // Performs the complex division with Scilab /
  r = x/y;
endfunction



//
// Search cases where Smith5 produce a different result from Li
// Often!

a=2^820, b=2^880, c=2^612, d=2^-612
  Smith= 4.114D+62
  Li= 4.114D+62
  Sm5= 4.114D+62-%i*1.42D-306

a=2^908, b=2^864, c=2^632, d=2^-632
  Smith= 1.214D+83
  Li= 1.214D+83
  Sm5= 1.214D+83-%i*3.82D-298


function [k , p] = searchForDiff ( nmatrix , kmax , div1 , div2 , verbose )
    //
    // Search for [a,b,c,d]=[2^na,2^nb,2^nc,2^nd]
    // such that a division function div1 gives a different 
    // result than div2.
    // nmatrix : a 4-by-2 matrix of floating point integers, the indices to use for na,nb,nc,nd. nmatrix(1,1:2) is the min/max for na, nmatrix(2,1:2) is the min/max for nb, nmatrix(3,1:2) is the min/max for nc, nmatrix(4,1:2) is the min/max for nd.
    // kmax : a 1-by-1 matrix of floating point integers, the number of Monte-Carlo experiments.
    // k : a 1-by-1 matrix of floating point integers, the number of times that Smith produced a different q than Naive
    // p : a 1-by-1 matrix of doubles, the probability that Smith produces a different result than naive. p=k/kmax

    na = grand(kmax,1,"uin",nmatrix(1,1),nmatrix(1,2))
    nb = grand(kmax,1,"uin",nmatrix(2,1),nmatrix(2,2))
    nc = grand(kmax,1,"uin",nmatrix(3,1),nmatrix(3,2))
    nd = grand(kmax,1,"uin",nmatrix(4,1),nmatrix(4,2))
    a=2^na
    b=2^nb
    c=2^nc
    d=2^nd
    x=a+%i*b
    y=c+%i*d
    k=0
    for i = 1 : kmax
        if ( verbose & modulo(i,10000) == 0 ) then
            mprintf("i=%d, k=%d, p=%.3e\n",i,k,k/i);
        end
        q1 = div1 ( x(i) , y(i) )
        q2 = div2 ( x(i) , y(i) )
        if ( q1 <> q2 & ..
            abs(real(q1))<>%inf & ..
            abs(imag(q1))<>%inf & ..
            ~isnan(real(q1)) & ..
            ~isnan(imag(q1)) ..
            ) then
            k=k+1
            if ( %f & verbose ) then
                mprintf("(2^%d+%%i*2^%d)/(2^%d+%%i*2^%d)\n",na(i),nb(i),nc(i),nd(i));
                mprintf("  Div1=%s+%%i*%s\n",string(real(q1)),string(imag(q1)))
                mprintf("  Div2=%s+%%i*%s\n",string(real(q2)),string(imag(q2)))
                pause
            end
        end
    end
    p = k/kmax
endfunction


//
// Search for differences between Naive and Scilab/x87:
// k=122503, kmax=1000000, p=1.225e-01

nmatrix = [
-1074 1023
-1074 1023
-1074 1023
-1074 1023
];
kmax = 1000000;
[k , p] = searchForDiff ( nmatrix , kmax , cdiv_naive , cdiv_scilab , %t );
mprintf("k=%d, kmax=%d, p=%.3e\n",k,kmax,p);


//
// Search for differences between Smith and Scilab/x87:
// k=13050, kmax=1000000, p=1.305e-02
// k=12923, kmax=1000000, p=1.292e-02

// Some examples :
(2^571+%i*2^-825)/(2^-1006+%i*2^306)
 Smith=                        -%i*5.928554968950589206D+79
 Scx87= 6.63123684676647598D-31-%i*5.928554968950589206D+79
 
(2^659+%i*2^-677)/(2^-740+%i*2^440)
 Smith=                        -%i*8.424983333484574936D+65
 Scx87= 5.13067100162297031D-29-%i*8.424983333484574936D+65
 
(2^980+%i*2^-949)/(2^441+%i*2^-896)
 Smith= 1.79956551781727855D+16
 Scx87= 1.79956551781727855D+16-%i*5.99878725558252381D-241
 
(2^911+%i*2^-405)/(2^214+%i*2^-948)
 Smith= 6.57516987693546688D+20+%i*4.59655735989167045D-187
 Scx87= 6.57516987693546688D+20-%i*1.04966814180735761D-140
 
(2^-782+%i*2^-909)/(2^-1070+%i*2^-1062)
 Smith= 7.588550360256754183D+8-%i*1.942668892225729071D+84
 Scx87= 7.588434569934336972D+8-%i*1.942639249903190265D+84
 
(2^629+%i*2^-1058)/(2^445+%i*2^-800)
 Smith= 2.451992865385422173D+5
 Scx87= 2.451992865385422173D+5-%i*4.04738577073149169D-320
 
nmatrix = [
-1074 1023
-1074 1023
-1074 1023
-1074 1023
];
kmax = 1000000;
[k , p] = searchForDiff ( nmatrix , kmax , cdiv_scilab , cdiv_smith , %t );
mprintf("k=%d, kmax=%d, p=%.3e\n",k,kmax,p);

//
// Search for differences between Smith5 and Scilab/x87 (extended precision on Linux 32 bits).
// k=240, kmax=1000000, p=2.400e-04
// k=201, kmax=1000000, p=2.010e-04
// k=219, kmax=1000000, p=2.190e-04

(2^-503+%i*2^-1071)/(2^-192+%i*2^-805)
   Sm5=2.397018293602405544D-94+%i*2.48104025832402391D-265 only 11 digits correct on imaginary part
 Scx87=2.397018293602405544D-94+%i*2.48104025832395340D-265 
 Exact=2.397018293602405543D-94+%i*2.48104025832395340D-265 
 
(2^-723+%i*2^-1068)/(2^-796+%i*2^-427)
   Sm5=1.09590467450420152D-193+%i*-7.854549544476362485D-90 Only 6 digits correct
 Scx87=1.09590473982521035D-193+%i*-7.854549544476362485D-90 
 Exact=1.09590473982521035D-193+%i*-7.854549544476362485D-90
 
(2^-135+%i*2^-152)/(2^720+%i*2^721)
   Sm5=8.32511669298081656D-259+%i*-1.66499158128085676D-258 1 digit lost
 Scx87=8.32511669298081541D-259+%i*-1.66499158128085653D-258 
 Exact=8.32511669298081564D-259+%i*-1.66499158128085658D-258
 
(2^-346+%i*2^-1062)/(2^-218+%i*2^-987)
   Sm5=2.938735877055718770D-39+%i*8.52478941564885960D-255
 Scx87=2.938735877055718770D-39+%i*8.52478941564885865D-255
 
(2^-1007+%i*2^-1033)/(2^-962+%i*2^-953)
   Sm5=1.084206308372789981D-19+%i*-5.551093947221322604D-17
 Scx87=1.084206308372789981D-19+%i*-5.551093947221321988D-17
 
(2^-936+%i*2^-1041)/(2^-217+%i*2^-73)
   Sm5=4.00833672001794555D-292+%i*-1.62597454369523231D-260
 Scx87=4.00833672002523667D-292+%i*-1.62597454369523231D-260
 Exact=4.00833672002523667D-292+%i*-1.62597454369523231D-260

nmatrix = [
-1074 1023
-1074 1023
-1074 1023
-1074 1023
];
kmax = 1000000;
[k , p] = searchForDiff ( nmatrix , kmax , cdiv_smith5 , cdiv_scilab , %t );
mprintf("k=%d, kmax=%d, p=%.3e\n",k,kmax,p);

//
// Search for differences between SmithLi and Scilab/x87 (extended precision on Linux 32 bits).
// k=13116, kmax=1000000, p=1.312e-02
// k=12854, kmax=1000000, p=1.285e-02

(2^-369+%i*2^946)/(2^451+%i*2^-765)
    Li=1.43022233380854697D-247+%i*1.02293456496754433D+149
 Scx87=9.06511099956111823D-218+%i*1.02293456496754433D+149
 

(2^997+%i*2^-855)/(2^-417+%i*2^767)
    Li=0.000000000000000000D+00+%i*-1.725436586697640947D+69
 Scx87=6.56725888207740199D-288+%i*-1.725436586697640947D+69
 

(2^-1053+%i*2^553)/(2^196+%i*2^-912)
    Li=0.000000000000000000D+00+%i*2.93567822846729153D+107
 Scx87=8.44254251528635457D-227+%i*2.93567822846729153D+107
 

(2^-628+%i*2^887)/(2^176+%i*2^-905)
    Li=9.37310508684769346D-243+%i*1.07727583263710689D+214
 Scx87=4.15816390625795968D-112+%i*1.07727583263710689D+214
 

(2^1023+%i*2^-733)/(2^775+%i*2^-435)
    Li=4.523128485832663884D+74+%i*0.000000000000000000D+00
 Scx87=4.523128485832663884D+74+%i*-2.56533550081148515D-290
 

(2^763+%i*2^-812)/(2^-924+%i*2^172)
    Li=6.11623645022269524D-297+%i*-8.10452259547068937D+177
 Scx87=9.54667613593626463D-153+%i*-8.10452259547068937D+177
 

(2^566+%i*2^-830)/(2^243+%i*2^-907)
    Li=1.708789628736728066D+97+%i*9.88131291682493088D-324
 Scx87=1.708789628736728066D+97+%i*-1.11736119828792732D-249
 

(2^-893+%i*2^912)/(2^584+%i*2^-602)
    Li=0.000000000000000000D+00+%i*5.468126811957529811D+98
 Scx87=5.20311853982474340D-259+%i*5.468126811957529811D+98
 
(2^-865+%i*2^765)/(2^-1044+%i*2^292)
    Li=2.43886605493436893D+142+%i*0.000000000000000000D+00
 Scx87=2.43886605493436893D+142+%i*1.62597454369523231D-260


kmax = 1000000;
[k , p] = searchForDiff ( nmatrix , kmax , cdiv_smithLi , cdiv_scilab , %t );
mprintf("k=%d, kmax=%d, p=%.3e\n",k,kmax,p);


//
// Search for differences between Smith4 and Scilab/x87 (extended precision on Linux 32 bits).
// k=222, kmax=1000000, p=2.220e-04
kmax = 1000000;
[k , p] = searchForDiff ( nmatrix , kmax , cdiv_smith4 , cdiv_scilab , %t );
mprintf("k=%d, kmax=%d, p=%.3e\n",k,kmax,p);



function searchForDiffWorst ( nmatrix , kmax , div1 , div2 , verbose )
    //
    // Search for [a,b,c,d]=[2^na,2^nb,2^nc,2^nd]
    // such that a division function div1 gives the worst possible 
    // number of correct digits with respect to div2, where 
    // div2 is assumed to be exact.
    // nmatrix : a 4-by-2 matrix of floating point integers, the indices to use for na,nb,nc,nd. nmatrix(1,1:2) is the min/max for na, nmatrix(2,1:2) is the min/max for nb, nmatrix(3,1:2) is the min/max for nc, nmatrix(4,1:2) is the min/max for nd.
    // kmax : a 1-by-1 matrix of floating point integers, the number of Monte-Carlo experiments.
    // k : a 1-by-1 matrix of floating point integers, the number of times that Smith produced a different q than Naive
    // p : a 1-by-1 matrix of doubles, the probability that Smith produces a different result than naive. p=k/kmax

    na = grand(kmax,1,"uin",nmatrix(1,1),nmatrix(1,2))
    nb = grand(kmax,1,"uin",nmatrix(2,1),nmatrix(2,2))
    nc = grand(kmax,1,"uin",nmatrix(3,1),nmatrix(3,2))
    nd = grand(kmax,1,"uin",nmatrix(4,1),nmatrix(4,2))
    a=2^na
    b=2^nb
    c=2^nc
    d=2^nd
    x=a+%i*b
    y=c+%i*d
    k = 0
    worstI = 0
    worstDigits = %inf
    for i = 1 : kmax
        if ( verbose & modulo(i,10000) == 0 ) then
            mprintf("i=%d, k=%d, p=%.3e\n",i,k,k/i);
        end
        q1 = div1 ( x(i) , y(i) )
        q2 = div2 ( x(i) , y(i) )
        if ( q1 <> q2 & ..
            abs(real(q1))<>%inf & ..
            abs(imag(q1))<>%inf & ..
            ~isnan(real(q1)) & ..
            ~isnan(imag(q1)) ..
            ) then
            k = k+1
            digits = assert_computedigits(q1,q2,2);
            if ( worstDigits > digits ) then
              worstDigits = digits
              worstI = i
            if ( verbose ) then
                mprintf("Worst case found - digits=%d\n",digits);
                mprintf("(2^%d+%%i*2^%d)/(2^%d+%%i*2^%d)\n",na(i),nb(i),nc(i),nd(i));
                mprintf("  Div1=%s+%%i*%s\n",string(real(q1)),string(imag(q1)))
                mprintf("  Div2=%s+%%i*%s\n",string(real(q2)),string(imag(q2)))
                pause
            end
            end
        end
    end
endfunction
//
// Search for worst accuracy for Smith5 vs Scilab/x87 (extended precision on Linux 32 bits).
kmax = 1000000;
searchForDiffWorst ( nmatrix , kmax , cdiv_smith5 , cdiv_scilab , %t );

Worst case found - digits=6
(2^-1073+%i*2^-518)/(2^-885+%i*2^-323)
   Sm5=1.991364888915565346D-59+%i*-1.68850850305727091D-226
 Scx87=1.991364888915565346D-59+%i*-1.67531703037713598D-226

Worst case found - digits=0
(2^-71+%i*2^1021)/(2^1001+%i*2^-323)
   Sm5=0.000000000000000000D+00+%i*1.048576000000000000D+06
 Scx87=1.97626258336498617D-323+%i*1.048576000000000000D+06

