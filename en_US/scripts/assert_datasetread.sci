// Copyright (C) 2010 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function mat = assert_datasetread ( filename , commenttag , sep , verbose )
    v = mgetl(filename)
    nrows = size(v,"r")
    if ( verbose ) then
        mprintf("Dataset : %s\n",filename)
        mprintf("Number of lines : %d\n",nrows)
    end
    //
    // Remove comment lines
    for i = 1 : size(v,'*')
        line = v(i);
        firstchar = part(line,1)
        if ( firstchar == commenttag ) then
            v(i) = ""
        end
    end
    //
    // Remove empty lines
    v(v == '') = [];
    //
    ns = length(sep);
    mat = [];
    ki = 1;
    for i = 1 : size(v,'*')
        line = v(i);
        K = [strindex(line, sep)];
        ki = 1;
        row = [];
        for k = K
            row = [row, part(line,ki:k-1)];
            ki = k + ns;
        end
        row = [row, part(line, ki:length(line))];
        if (i > 1) then
            if ( size(row,2) > size(mat,2) ) then
                mat($,size(row,2)) = '';
            elseif ( size(row,2) < size(mat,2) ) then
                row(1, size(mat,2)) = '';
            end
        end
        mat = [mat; row];
    end
    mat = strsubst(mat,"I","%i")
    mat = strsubst(mat,"nan","%nan")
    mat = strsubst(mat,"infinity","%inf")
endfunction

