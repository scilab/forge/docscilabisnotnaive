// Copyright (C) 2010 - Michael Baudin

// An attempt at implementing the complex division as
// in C99

function r = cdiv_ansiisoC(z,w)
    //  ISO/IEC 9899:1999 (E) �ISO/IEC
    // Copyright (C) 2010 - Michael Baudin
    ilogbw = 0
    a = real(z)
    b = imag(z)
    c = real(w)
    d = imag(w)
    [f,logbw] = frexp(max(abs(c), abs(d)))
    if (isfinite(logbw)) then
        ilogbw = round(logbw)
        c = pow2(c, -ilogbw)
        d = pow2(d, -ilogbw)
    end
    den = c * c + d * d
    x = pow2((a * c + b * d) / den, -ilogbw)
    y = pow2((b * c - a * d) / den, -ilogbw)
    // Recover infinities and zeros that computed as NaN+iNaN */
    // the only cases are nonzero/zero, infinite/finite, and finite/infinite, ... */
    if (isnan(x) & isnan(y)) then
        if ((den == 0.0) & (~isnan(a) | ~isnan(b))) then
            x = copysign(%inf, c) * a
            y = copysign(%inf, c) * b
        elseif ((isinf(a) | isinf(b)) & isfinite(c) & isfinite(d)) then
            if (isinf(a)) then
                a = copysign(1, a)
            else
                a = copysign(0, a)
            end
            if ( isinf(b) ) then
                b = copysign(1, b)
            else
                b = copysign(0, b)
            end
            x = %inf * ( a * c + b * d )
            y = %inf * ( b * c - a * d )
        elseif (isinf(logbw) & isfinite(a) & isfinite(b)) then
            if ( isinf(c) ) then
                c = copysign(1, c)
            else
                c = copysign(0, c)
            end
            if ( isinf(d) ) then
                d = copysign(1, d)
            else
                d = copysign(0, d)
            end
            x = 0.0 * ( a * c + b * d )
            y = 0.0 * ( b * c - a * d )
        end
    end
    // Avoid using x + %i * y, which may create 
    // %inf+%i*%inf, which actually multiplies 0 and %inf, 
    // generating an unwanted %nan.
    r = complex(x,y)
endfunction

if ( %f ) then

    // 11/25 + %i* 2/25
    r = cdiv_ansiisoC(1+%i*2,3+%i*4) 
    // A case which fails : should be 1e100 - %i * 1e-300
    cdiv_ansiisoC(1e300+%i*1e-300,1e200+%i*1e-200)
end

exec("pow2.sci");
exec("copysign.sci");
exec("isfinite.sci");

