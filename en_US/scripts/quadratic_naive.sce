// Copyright (C) 2009 - Michael Baudin

// An naive implementation of the roots of a quadratic equation
function r = myroots(p)
  c=coeff(p,0);
  b=coeff(p,1);
  a=coeff(p,2);
  r(1)=(-b+sqrt(b^2-4*a*c))/(2*a);
  r(2)=(-b-sqrt(b^2-4*a*c))/(2*a);
endfunction
function compare ( a , b , c )
  p=poly([c b a],"x","coeff");
  roots1 = myroots(p);
  roots2 = roots(p);
  del = b^2-4*a*c;
  printf("Delta : %e \n", del);
  printf("Naive method : %s %s\n", string(roots1(1)) , string(roots1(2)));
  printf("Scilab method : %s %s\n", string(roots2(1)) , string(roots2(2)));
endfunction
// Basic check
compare ( 1 , 5 , 1 );
compare ( 1 , 2 , 1 );
// Example #1 : massive cancelation
// Check that the naive method does not work on this quadratic 
// -0.0001 x^2 + 10000.0 x + 0.0001 = 0
e = 0.0001;
a = e;
b = 1/e;
c = -e;
p=poly([c b a],"x","coeff");
e1 = 1e-8;
roots1 = myroots(p);
r1 = roots1(1);
roots2 = roots(p);
r2 = roots2(1);
error1 = abs(r1-e1)/e1;
error2 = abs(r2-e1)/e1;
printf("Expected : %e\n", e1);
printf("Naive method : %e (error=%e)\n", r1,error1);
printf("Scilab method : %e (error=%e)\n", r2, error2);
// Example #2 : overflow in the discriminant
e=1.e-155
a = 1;
b = 1/e;
c = 1;
p=poly([c b a],"x","coeff");
expected = [-e;-1/e];
roots1 = myroots(p);
roots2 = roots(p);
error1 = abs(roots1-expected)/norm(expected);
error2 = abs(roots2-expected)/norm(expected);
printf("Expected : %e %e\n", expected(1),expected(2));
printf("Naive method : %e %e (error=%e %e)\n", roots1(1),roots1(2),error1(1),error1(2));
printf("Scilab method : %e %e (error=%e %e)\n", roots2(1),roots2(2), error2(1),error2(2));

// An improved implementation of the roots of a quadratic equation
function r = myroots2 (p)
  c=coeff(p,0);
  b=coeff(p,1);
  a=coeff(p,2);
  if ( a == 0 ) then
    if ( b == 0 ) then
       r(1) = 0
       r(2) = 0
     else
       r(1) = -c/b
       r(2) = 0
     end
     return
   end
   // The general case
   bp = b/2
   [d,s]=discriminant(a,bp,c)
   if ( s < 0 ) then
     r(1) = -bp/a + %i * s / a
     r(2) = -bp/a - %i * s / a
     return
   elseif ( s == 0 ) then
     r(1) = -bp/a
     r(2) = r(1)
     return
   end
   // Delta > 0
   if ( b > 0 ) then
     g = 1
   else
     g = -1
   end
   h = -(bp + g * d)
   r(1) = c / h
   r(2) = h / a
endfunction

function compare2 ( a , b , c )
  p=poly([c b a],"x","coeff");
  roots1 = myroots2(p);
  roots2 = roots(p);
  del = b^2-4*a*c;
  printf("Delta : %e \n", del);
  printf("Not Naive method : %s %s\n", string(roots1(1)) , string(roots1(2)));
  printf("Scilab method : %s %s\n", string(roots2(1)) , string(roots2(2)));
endfunction
// Basic check
compare2 ( 1 , 5 , 1 );
compare2 ( 1 , 2 , 1 );
compare2 ( 1 , 2 , 3 );
// Example #1 : massive cancelation
// Check that the naive method does not work on this quadratic 
// -0.0001 x^2 + 10000.0 x + 0.0001 = 0
e = 0.0001;
a = e;
b = 1/e;
c = -e;
p=poly([c b a],"x","coeff");
e1 = 1e-8;
roots1 = myroots2(p);
r1 = roots1(1);
roots2 = roots(p);
r2 = roots2(1);
error1 = abs(r1-e1)/e1;
error2 = abs(r2-e1)/e1;
printf("Expected : %e\n", e1);
printf("Naive method : %e (error=%e)\n", r1,error1);
printf("Scilab method : %e (error=%e)\n", r2, error2);
// Example #2 : overflow in the discriminant
e=1.e-155
a = 1;
b = 1/e;
c = 1;
p=poly([c b a],"x","coeff");
expected = [-e;-1/e];
roots1 = myroots2(p);
roots2 = roots(p);
error1 = abs(roots1-expected)/norm(expected);
error2 = abs(roots2-expected)/norm(expected);
printf("Expected : %e %e\n", expected(1),expected(2));
printf("Naive method : %e %e (error=%e %e)\n", roots1(1),roots1(2),error1(1),error1(2));
printf("Scilab method : %e %e (error=%e %e)\n", roots2(1),roots2(2), error2(1),error2(2));
// Example #3 : simple experiment
p=poly([-2.,-1.,1.],"x","coeff")
expected = [-1;2.];
roots1 = myroots2(p);
roots2 = roots(p);
error1 = abs(roots1-expected)/norm(expected);
error2 = abs(roots2-expected)/norm(expected);
printf("Expected : %e %e\n", expected(1),expected(2));
printf("Naive method : %e %e (error=%e %e)\n", roots1(1),roots1(2),error1(1),error1(2));
printf("Scilab method : %e %e (error=%e %e)\n", roots2(1),roots2(2), error2(1),error2(2));

//*****************************************************************
// A double root, difficult to distinguish
a = 1
r = 123456789
b = -246913578
// Exact result
//c=15241578750190521
c = 15241578750190520
d = b^2 - 4*a*c
p=poly([c b a],"x","coeff");
r1=roots(p)
// The same problem, slightly perturbed
a = 1
r = 123456789
b = -246913578
// Exact result
//c=15241578750190521
c = 15241578750190521-10
d = b^2 - 4*a*c
p=poly([c b a],"x","coeff");
r2=roots(p)
// The same problem, slightly perturbed
a = 1
r = 123456789
b = -246913578
c = 15241578750190519
d = b^2 - 4*a*c
p=poly([c b a],"x","coeff");
r3=roots(p)
// Mathematica results 
// (246913578 - sqrt(246913578^2-4*15241578750190519))/2
// (246913578 + sqrt(246913578^2-4*15241578750190519))/2
// 123456787.585786437626904951198311275790301921430328124
// 123456790.414213562373095048801688724209698078569671875
// Scilab results
// 123456787.16035048663616
// 123456790.83964951336384
// Relative errors :
// 3.446D-09 > 8 digits accurate (7 digits lost)
// 3.446D-09 > 8 digits accurate (7 digits lost)
// Let's see the condition number of the eigenvectors of the 
// companion matrix
A= companion(p)
[R,diagevals]=spec(A)
cond(R)
//*****************************************************************
// 
a = 94906265.625 
b = -2 * 94906267.000 
c = 94906268.375
d = b^2 - 4*a*c
// Exact solutions 
// 1.000000028975958
// 1.0
p=poly([c b a],"x","coeff");
r=roots(p)
//*****************************************************************

a = 94906266.375 
b = -2 * 94906267.375 
c = 94906268.375
// 1.000000021073424
// 1.0
p=poly([c b a],"x","coeff");
r=roots(p)
//*****************************************************************


function [d,s]=discriminant(a,b,c)
    // if b**2-a*c>0, computes d=sqrt(b**2-a*c), s=+1
    // if b**2-a*c<0, computes d=sqrt(a*c-b**2), s=-1
    // if b**2-a*c==0, computed d=0, s=0
    // Avoids overflows.
    if (b==0 & c==0) then
        d=0
        s=0
        return
    end
    if ( abs(b) > abs(c) ) then
        e = 1 - (a / b) * (c / b)
        d = abs(b) * sqrt(abs(e))
    else
        e = b * (b/c) - a
        d = sqrt(abs(c)) * sqrt(abs(e))
    end
    if (e<0) then
        s=-1
    elseif e==0 then
        s=0
    else
        s=1
    end
    if ( abs(b) <= abs(c) ) then
        s=s*sign(c)
    end
endfunction
function [d,s]=discriminantNaive(a,b,c)
    // if b**2-a*c>0, computes d=sqrt(b**2-a*c), s=+1
    // if b**2-a*c<0, computes d=sqrt(a*c-b**2), s=-1
    // if b**2-a*c==0, computed d=0, s=0
    // Avoids overflows.
    d=b**2-a*c
    if d>0 then
        d=sqrt(d)
        s=1
    elseif d==0 then
        s=0
    else
        d=sqrt(-d)
        s=-1
    end
endfunction
for i=1:1000
    a=grand(1, 1, "uin", -10, 10);
    b=grand(1, 1, "uin", -10, 10);
    c=grand(1, 1, "uin", -10, 10);
    [d1,s1]=discriminant(a,b,c);
    [d2,s2]=discriminantNaive(a,b,c);
    printf("#%d, s=(%d,%d),d=(%f,%f)\n",i,s1,s2,d2,d2)
    if (s1<>s2) then
        printf("a=%d",a)
        printf("b=%d",b)
        printf("c=%d",c)
        printf("Sign is different !")
    end
    if (abs(s1-s2)>%eps) then
        printf("a=%d",a)
        printf("b=%d",b)
        printf("c=%d",c)
        printf("Discriminant is different !")
        printf("d1=%d",d1)
        printf("d2=%d",d2)
    end
end

a=1
b=-1
c=-2
b=b/2
s=sqrt(b**2-a*c)
x1=-c/(b+sign(b)*s)
x2=-(b+sign(b)*s)/a
