# Copyright (C) 2009 - Michael Baudin
# Octave version
# http://en.wikipedia.org/wiki/Methods_of_computing_square_roots
# http://mathworld.wolfram.com/SquareRootAlgorithms.html
# Straitforward implementation
function mn2 = mynorm2(a,b)
  mn2 = sqrt(a^2+b^2);
endfunction
# With scaling
function mn2 = mypythag1(a,b)
  if (a==0.0) then
    mn2 = abs(b);
  elseif (b==0.0) then
    mn2 = abs(a);
  else
    if (abs(b)>abs(a)) then
      r = a/b;
      t = abs(b);
    else
      r = b/a;
      t = abs(a);
    end
    mn2 = t * sqrt(1 + r^2);
  end
endfunction
# With Moler & Morrison's
# At most 7 iterations are required.
function mn2 = mypythag2(a,b)
  p = max(abs(a),abs(b));
  q = min(abs(a),abs(b));
  #index = 0
  while (q!=0.0)
    #index = index + 1
    #mprintf("index = %d, p = %e, q = %e\n",index,p,q)
    r = (q/p)^2;
    s = r/(4+r);
    p = p + 2*s*p;
    q = s * q;
  end
  mn2 = p;
endfunction
function compare(x)
  printf("x(1)=%e, x(2)=%e\n",x(1),x(2));
  p = norm(x,2);
  printf("%20s : %e\n","Octave",p);
  p = mynorm2(x(1),x(2));
  printf("%20s : %e\n","Naive",p);
  p = mypythag1(x(1),x(2));
  printf("%20s : %e\n","Scaling",p);
  p = mypythag2(x(1),x(2));
  printf("%20s : %e\n","Moler & Morrison",p);
endfunction
# Test #1 : all is fine
x = [1 1]
compare(x);
# Test #2 : more difficult when x is large
x = [1.e200 1]
compare(x)
# Test #3 : more difficult when x is small
x = [1.e-200 1.e-200]
compare(x)


